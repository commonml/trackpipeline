import os
import sys
import platform
import getopt
from subprocess import call
from shutil import copyfile

def main():
    orientationFilePath = "orientation.json"
    tracksFilePath = "tracks.csv"

    try:
        opts, args = getopt.getopt(sys.argv[1:], "ho:t:", ["help", "orientation", "tracks"])
    except getopt.GetoptError as err:
        print(err)
        print("print --help to read usage of the programm")
        sys.exit(2)
    
    for o, a in opts:
        if o in ("-h", "--help"):
            print(__doc__)
            sys.exit(0)
        if o in ("-o", "--orientation"):
            orientationFilePath = a
        if o in ("-t", "--tracks"):
            tracksFilePath = a


    # TODO:
    # orientation
    call(["python", "../orientation/equitangular.py", "-i", orientationFilePath])
    # mapping with unity
    orientationResultFilePath = orientationFilePath + ".rot"
    mappingTracksOrientationFilePath = "camera.rot"
    if not orientationResultFilePath == mappingTracksOrientationFilePath:
        copyfile(orientationResultFilePath, mappingTracksOrientationFilePath)
    mappingTracksFilePath = "tracks.csv"
    mappingTracksOnMapFilePath = "track360Out.csv"
    if not tracksFilePath == mappingTracksFilePath:
        copyfile(tracksFilePath, mappingTracksFilePath)
    if platform.system() == "Darwin":
        call(["/usr/bin/open", "-W", "-n", "-a", "mapping.app"])
    elif platform.system() == "Linux":
        call(["mapping_linux.x86"])
    elif platform.system() == "Windows":
        call([os.path.join("mapping_windows", "Rectangular")])
    mappedTracksFilePath = tracksFilePath + ".mapped"
    if not mappedTracksFilePath == mappingTracksOnMapFilePath:
        copyfile(mappingTracksOnMapFilePath, mappedTracksFilePath)
    # mapping visualisation
    mappedTracksImageFilePath = tracksFilePath + ".mapped.png"
    call(["python", "../clusterization/display.py", "-i", mappingTracksOnMapFilePath, "-o", mappedTracksImageFilePath, "-t", "car"])
    # clusterisation
    clusteredTracksFilePath = tracksFilePath + ".cluster"
    call(["python", "../clusterization/trek_graph.py", mappingTracksOnMapFilePath, "car", clusteredTracksFilePath])
    # clusterisation visualisation
    clusteredTracksImageFilePath = tracksFilePath + ".cluster.png"
    call(["python", "../clusterization/display.py", "-l", "-i", clusteredTracksFilePath, "-o", clusteredTracksImageFilePath, "-t", "car"])

if __name__ == "__main__":
    main()
